﻿using System;
using System.Linq;
using System.Collections.Generic;
using Common;
using NUnit.Framework;

namespace NUnit
{
    [TestFixture]
    public partial class RedBlackTreeFixture
    {
        Random random = new Random();

        [Test]
        public void TestBasicFeatures()
        {
            var count = 1000;
            var keys = new List<Int32>(count);
            var tree = new RBTree<Int32, String>();
            var dictionary = new SortedDictionary<Int32, String>();

            for (int i = 0; i < count; i++)
            {
                var value = random.Next();

                keys.Add(value);
                tree.Add(value, value.ToString());
                dictionary.Add(value, value.ToString());
            }

            keys.Sort();

            Assert.IsTrue(tree.Count == dictionary.Count, "tree.count is not equal to dictionary count");

            {
                var key = keys.First() - 1;
                var nearest = tree.NearestNodes(key);

                Assert.IsTrue(nearest.A == null);
                Assert.IsTrue(nearest.B != null && nearest.B.Key == key + 1);
            }

            {
                var key = keys.Last() + 1;
                var nearest = tree.NearestNodes(key);
                Assert.IsTrue(nearest.A != null && nearest.A.Key == key - 1);
                Assert.IsTrue(nearest.B == null);
            }

            for (int i = 0; i < count; i++)
            {
                var key = keys[i];

                Assert.IsTrue(tree.ContainsKey(key));
                Assert.IsTrue(tree.FindNode(key) != null && tree.FindNode(key).Key == key);

                var nearest = tree.NearestNodes(key);

                Assert.IsTrue(nearest.A.Key == nearest.B.Key && nearest.A.Value == nearest.B.Value);
            }

        }
    }
}
