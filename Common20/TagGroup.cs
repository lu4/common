using System;
using System.Text;
using System.Text.RegularExpressions;

namespace Common
{
    public class TagGroup
    {
        public TagGroup(params String[] patterns)
            : this(true, patterns)
        {

        }

        public TagGroup(Boolean escape, params String[] patterns)
        {
            Tags = new Tag[patterns.Length];

            for (var i = 0; i < patterns.Length; i++)
            {
                Tags[i] = new Tag(patterns[i], escape);
            }
        }

        public TagGroup(params Tag[] tags)
        {
            this.Tags = tags;
        }

        public Tag[] Tags { get; private set; }

        public String GetRegexPattern()
        {
			StringBuilder sb = new StringBuilder();
			
			sb.Append("(?>");
			
			if (Tags.Length > 0)
			{
				sb.Append(Tags[0].GetRegexPattern());
				
				for (int i = 1; i < Tags.Length; i++)
				{
					sb.Append("|");
					sb.Append(Tags[i].GetRegexPattern());
				}
			}
			
			sb.Append(")");
			
            return sb.ToString();
        }

        private Regex RegularExpression;
        private RegexOptions RegexOptions;
        public String Parse(RegexOptions regexOptions, String input, String value)
        {
            if (RegularExpression == null || RegexOptions != regexOptions)
            {
                RegexOptions = regexOptions;
                RegularExpression = new Regex(GetRegexPattern(), RegexOptions = regexOptions);
            }

            return RegularExpression.Replace(input, value);
        }
    }
}

