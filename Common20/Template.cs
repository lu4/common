using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Common
{
    public class Template : IEnumerable<KeyValuePair<TagGroup, Object>>, IEnumerable<KeyValuePair<SectionGroup, IEnumerable<Template>>>
    {
        private Dictionary<TagGroup, Object> tags = new Dictionary<TagGroup, Object>();
        private Dictionary<SectionGroup, IEnumerable<Template>> sections = new Dictionary<SectionGroup, IEnumerable<Template>>();



        public void Add(String value0, Object obj)
        {
            tags.Add(new TagGroup(value0), obj);
        }
        public void Add(Boolean escape, String value0, Object obj)
        {
            tags.Add(new TagGroup(escape, value0), obj);
        }
        public void Add(Tag value0, Object obj)
        {
            tags.Add(new TagGroup(value0), obj);
        }
        public void Add(Section value0, params Template[] templates)
        {
            sections.Add(new SectionGroup(value0), templates);
        }
        public void Add(Section value0, IEnumerable<Template> templates)
        {
            sections.Add(new SectionGroup(value0), templates);
        }

        public void Add(String value0, String value1, Object obj)
        {
            tags.Add(new TagGroup(value0, value1), obj);
        }
        public void Add(Boolean escape, String value0, String value1, Object obj)
        {
            tags.Add(new TagGroup(escape, value0, value1), obj);
        }
        public void Add(Tag value0, Tag value1, Object obj)
        {
            tags.Add(new TagGroup(value0, value1), obj);
        }
        public void Add(Section value0, Section value1, params Template[] templates)
        {
            sections.Add(new SectionGroup(value0, value1), templates);
        }
        public void Add(Section value0, Section value1, IEnumerable<Template> templates)
        {
            sections.Add(new SectionGroup(value0, value1), templates);
        }

        public void Add(String value0, String value1, String value2, Object obj)
        {
            tags.Add(new TagGroup(value0, value1, value2), obj);
        }
        public void Add(Boolean escape, String value0, String value1, String value2, Object obj)
        {
            tags.Add(new TagGroup(escape, value0, value1, value2), obj);
        }
        public void Add(Tag value0, Tag value1, Tag value2, Object obj)
        {
            tags.Add(new TagGroup(value0, value1, value2), obj);
        }
        public void Add(Section value0, Section value1, Section value2, params Template[] templates)
        {
            sections.Add(new SectionGroup(value0, value1, value2), templates);
        }
        public void Add(Section value0, Section value1, Section value2, IEnumerable<Template> templates)
        {
            sections.Add(new SectionGroup(value0, value1, value2), templates);
        }

        public void Add(String value0, String value1, String value2, String value3, Object obj)
        {
            tags.Add(new TagGroup(value0, value1, value2, value3), obj);
        }
        public void Add(Boolean escape, String value0, String value1, String value2, String value3, Object obj)
        {
            tags.Add(new TagGroup(escape, value0, value1, value2, value3), obj);
        }
        public void Add(Tag value0, Tag value1, Tag value2, Tag value3, Object obj)
        {
            tags.Add(new TagGroup(value0, value1, value2, value3), obj);
        }
        public void Add(Section value0, Section value1, Section value2, Section value3, params Template[] templates)
        {
            sections.Add(new SectionGroup(value0, value1, value2, value3), templates);
        }
        public void Add(Section value0, Section value1, Section value2, Section value3, IEnumerable<Template> templates)
        {
            sections.Add(new SectionGroup(value0, value1, value2, value3), templates);
        }

        public void Add(String value0, String value1, String value2, String value3, String value4, Object obj)
        {
            tags.Add(new TagGroup(value0, value1, value2, value3, value4), obj);
        }
        public void Add(Boolean escape, String value0, String value1, String value2, String value3, String value4, Object obj)
        {
            tags.Add(new TagGroup(escape, value0, value1, value2, value3, value4), obj);
        }
        public void Add(Tag value0, Tag value1, Tag value2, Tag value3, Tag value4, Object obj)
        {
            tags.Add(new TagGroup(value0, value1, value2, value3, value4), obj);
        }
        public void Add(Section value0, Section value1, Section value2, Section value3, Section value4, params Template[] templates)
        {
            sections.Add(new SectionGroup(value0, value1, value2, value3, value4), templates);
        }
        public void Add(Section value0, Section value1, Section value2, Section value3, Section value4, IEnumerable<Template> templates)
        {
            sections.Add(new SectionGroup(value0, value1, value2, value3, value4), templates);
        }

        public void Add(String value0, String value1, String value2, String value3, String value4, String value5, Object obj)
        {
            tags.Add(new TagGroup(value0, value1, value2, value3, value4, value5), obj);
        }
        public void Add(Boolean escape, String value0, String value1, String value2, String value3, String value4, String value5, Object obj)
        {
            tags.Add(new TagGroup(escape, value0, value1, value2, value3, value4, value5), obj);
        }
        public void Add(Tag value0, Tag value1, Tag value2, Tag value3, Tag value4, Tag value5, Object obj)
        {
            tags.Add(new TagGroup(value0, value1, value2, value3, value4, value5), obj);
        }
        public void Add(Section value0, Section value1, Section value2, Section value3, Section value4, Section value5, params Template[] templates)
        {
            sections.Add(new SectionGroup(value0, value1, value2, value3, value4, value5), templates);
        }
        public void Add(Section value0, Section value1, Section value2, Section value3, Section value4, Section value5, IEnumerable<Template> templates)
        {
            sections.Add(new SectionGroup(value0, value1, value2, value3, value4, value5), templates);
        }

        public void Add(String value0, String value1, String value2, String value3, String value4, String value5, String value6, Object obj)
        {
            tags.Add(new TagGroup(value0, value1, value2, value3, value4, value5, value6), obj);
        }
        public void Add(Boolean escape, String value0, String value1, String value2, String value3, String value4, String value5, String value6, Object obj)
        {
            tags.Add(new TagGroup(escape, value0, value1, value2, value3, value4, value5, value6), obj);
        }
        public void Add(Tag value0, Tag value1, Tag value2, Tag value3, Tag value4, Tag value5, Tag value6, Object obj)
        {
            tags.Add(new TagGroup(value0, value1, value2, value3, value4, value5, value6), obj);
        }
        public void Add(Section value0, Section value1, Section value2, Section value3, Section value4, Section value5, Section value6, params Template[] templates)
        {
            sections.Add(new SectionGroup(value0, value1, value2, value3, value4, value5, value6), templates);
        }
        public void Add(Section value0, Section value1, Section value2, Section value3, Section value4, Section value5, Section value6, IEnumerable<Template> templates)
        {
            sections.Add(new SectionGroup(value0, value1, value2, value3, value4, value5, value6), templates);
        }

        public void Add(String value0, String value1, String value2, String value3, String value4, String value5, String value6, String value7, Object obj)
        {
            tags.Add(new TagGroup(value0, value1, value2, value3, value4, value5, value6, value7), obj);
        }
        public void Add(Boolean escape, String value0, String value1, String value2, String value3, String value4, String value5, String value6, String value7, Object obj)
        {
            tags.Add(new TagGroup(escape, value0, value1, value2, value3, value4, value5, value6, value7), obj);
        }
        public void Add(Tag value0, Tag value1, Tag value2, Tag value3, Tag value4, Tag value5, Tag value6, Tag value7, Object obj)
        {
            tags.Add(new TagGroup(value0, value1, value2, value3, value4, value5, value6, value7), obj);
        }
        public void Add(Section value0, Section value1, Section value2, Section value3, Section value4, Section value5, Section value6, Section value7, params Template[] templates)
        {
            sections.Add(new SectionGroup(value0, value1, value2, value3, value4, value5, value6, value7), templates);
        }
        public void Add(Section value0, Section value1, Section value2, Section value3, Section value4, Section value5, Section value6, Section value7, IEnumerable<Template> templates)
        {
            sections.Add(new SectionGroup(value0, value1, value2, value3, value4, value5, value6, value7), templates);
        }

        public void Add(String value0, String value1, String value2, String value3, String value4, String value5, String value6, String value7, String value8, Object obj)
        {
            tags.Add(new TagGroup(value0, value1, value2, value3, value4, value5, value6, value7, value8), obj);
        }
        public void Add(Boolean escape, String value0, String value1, String value2, String value3, String value4, String value5, String value6, String value7, String value8, Object obj)
        {
            tags.Add(new TagGroup(escape, value0, value1, value2, value3, value4, value5, value6, value7, value8), obj);
        }
        public void Add(Tag value0, Tag value1, Tag value2, Tag value3, Tag value4, Tag value5, Tag value6, Tag value7, Tag value8, Object obj)
        {
            tags.Add(new TagGroup(value0, value1, value2, value3, value4, value5, value6, value7, value8), obj);
        }
        public void Add(Section value0, Section value1, Section value2, Section value3, Section value4, Section value5, Section value6, Section value7, Section value8, params Template[] templates)
        {
            sections.Add(new SectionGroup(value0, value1, value2, value3, value4, value5, value6, value7, value8), templates);
        }
        public void Add(Section value0, Section value1, Section value2, Section value3, Section value4, Section value5, Section value6, Section value7, Section value8, IEnumerable<Template> templates)
        {
            sections.Add(new SectionGroup(value0, value1, value2, value3, value4, value5, value6, value7, value8), templates);
        }

        public void Add(String value0, String value1, String value2, String value3, String value4, String value5, String value6, String value7, String value8, String value9, Object obj)
        {
            tags.Add(new TagGroup(value0, value1, value2, value3, value4, value5, value6, value7, value8, value9), obj);
        }
        public void Add(Boolean escape, String value0, String value1, String value2, String value3, String value4, String value5, String value6, String value7, String value8, String value9, Object obj)
        {
            tags.Add(new TagGroup(escape, value0, value1, value2, value3, value4, value5, value6, value7, value8, value9), obj);
        }
        public void Add(Tag value0, Tag value1, Tag value2, Tag value3, Tag value4, Tag value5, Tag value6, Tag value7, Tag value8, Tag value9, Object obj)
        {
            tags.Add(new TagGroup(value0, value1, value2, value3, value4, value5, value6, value7, value8, value9), obj);
        }
        public void Add(Section value0, Section value1, Section value2, Section value3, Section value4, Section value5, Section value6, Section value7, Section value8, Section value9, params Template[] templates)
        {
            sections.Add(new SectionGroup(value0, value1, value2, value3, value4, value5, value6, value7, value8, value9), templates);
        }
        public void Add(Section value0, Section value1, Section value2, Section value3, Section value4, Section value5, Section value6, Section value7, Section value8, Section value9, IEnumerable<Template> templates)
        {
            sections.Add(new SectionGroup(value0, value1, value2, value3, value4, value5, value6, value7, value8, value9), templates);
        }


        public void Add(TagGroup tagGroup, Object obj)
        {
            tags.Add(tagGroup, obj);
        }
        public void Add(SectionGroup sectionGroup, params Template[] templates)
        {
            sections.Add(sectionGroup, templates);
        }
        public void Add(SectionGroup sectionGroup, IEnumerable<Template> templates)
        {
            sections.Add(sectionGroup, templates);
        }

        public String Parse(RegexOptions regexOptions, String input)
        {
            foreach (var pair in sections)
            {
                var sectionGroup = pair.Key;
                var sectionTemplates = pair.Value;

                input = sectionGroup.Parse(regexOptions, input, sectionTemplates);
            }

            foreach (var pair in tags)
            {
                var tagGroup = pair.Key;
                var tagValue = pair.Value;

                input = tagGroup.Parse(regexOptions, input, tagValue.ToString());
            }

            return input;
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            foreach (var value in tags)
            {
                yield return value;
            }

            foreach (var value in sections)
            {
                yield return value;
            }
        }

        IEnumerator<KeyValuePair<TagGroup, object>> IEnumerable<KeyValuePair<TagGroup, object>>.GetEnumerator()
        {
            return tags.GetEnumerator();
        }
        IEnumerator<KeyValuePair<SectionGroup, IEnumerable<Template>>> IEnumerable<KeyValuePair<SectionGroup, IEnumerable<Template>>>.GetEnumerator()
        {
            return sections.GetEnumerator();
        }
    }
}

