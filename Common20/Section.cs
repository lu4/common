using System;
using System.Text.RegularExpressions;

namespace Common
{
    public class Section
    {
        public Section(String openingPattern, String closingPattern)
            : this(openingPattern, closingPattern, true)
        {
        }

        public Section(String openingPattern, String closingPattern, Boolean escape)
        {
            this.Escape = escape;
            this.OpeningPattern = openingPattern;
            this.ClosingPattern = closingPattern;
        }

        public Boolean Escape { get; private set; }
        public String OpeningPattern { get; private set; }
        public String ClosingPattern { get; private set; }

        public String GetRegexPattern()
        {
            return
            "(?>"
                + (Escape ? Regex.Escape(OpeningPattern) : OpeningPattern)
                + "(?<Content>.*?)"
                + (Escape ? Regex.Escape(ClosingPattern) : ClosingPattern) +
            ")";
        }
    }
}

