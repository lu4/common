using System;
using System.Text;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Common
{
    public class SectionGroup
    {
        private class MatchEvaluator
        {
            public MatchEvaluator(IEnumerable<Template> templates, RegexOptions regexOptions)
            {
                this.Templates = templates;
                this.RegexOptions = regexOptions;
            }

            public RegexOptions RegexOptions { get; private set; }

            public String EvaluateMatch(Match match)
            {
                var content = match.Groups["Content"].Value;

                StringBuilder sb = new StringBuilder();

                foreach (var template in Templates)
                {
                    sb.Append(template.Parse(RegexOptions, content));
                }

                return sb.ToString();
            }

            public IEnumerable<Template> Templates { get; private set; }
        }

        public SectionGroup(params Section[] sections)
        {
            this.Sections = sections;
        }

        public SectionGroup(params String[] strings)
            : this(true, strings)
        {
        }

        public SectionGroup(Boolean escape, params String[] strings)
        {
            if (strings.Length % 2 == 0)
            {
                Sections = new Section[strings.Length >> 1];

                for (int i = 0; i < strings.Length; i += 2)
                {
                    Sections[i >> 1] = new Section(strings[i], strings[i + 1], escape);
                }
            }
            else
            {
                throw new ArgumentException("strings's length must be multiple of 2");
            }
        }

        public Section[] Sections { get; private set; }

        public String GetRegexPattern()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("(?>");

            if (Sections.Length > 0)
            {
                sb.Append(Sections[0].GetRegexPattern());

                for (int i = 1; i < Sections.Length; i++)
                {
                    sb.Append("|");
                    sb.Append(Sections[i].GetRegexPattern());
                }
            }

            sb.Append(")");

            return sb.ToString();
        }

        private Regex RegularExpression;
        private RegexOptions RegexOptions;
        public String Parse(RegexOptions regexOptions, String input, IEnumerable<Template> templates)
        {
            if (RegularExpression == null || RegexOptions != regexOptions)
            {
                RegexOptions = regexOptions;
                RegularExpression = new Regex(GetRegexPattern(), RegexOptions = regexOptions);
            }

            var evaluator = new MatchEvaluator(templates, regexOptions);

            return RegularExpression.Replace(input, evaluator.EvaluateMatch);
        }
    }
}