﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    // 
    public class ValueEventArgs<T> : EventArgs
    {
        public ValueEventArgs(T value)
        {
            this.Value = value;
        }

        public T Value { get; private set; }
    }
    public class NewValueEventArgs<T> : EventArgs
    {
        public NewValueEventArgs(T nextValue)
        {
            this.NewValue = nextValue;
        }

        public T NewValue { get; private set; }
    }
    public class OldValueEventArgs<T> : EventArgs
    {
        public OldValueEventArgs(T previousValue)
        {
            this.OldValue = previousValue;
        }

        public T OldValue { get; private set; }
    }
    public class OriginalValueEventArgs<T> : EventArgs
    {
        public OriginalValueEventArgs(T previousValue)
        {
            this.OriginalValue = previousValue;
        }

        public T OriginalValue { get; private set; }
    }

    
}
