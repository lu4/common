﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace Common
{
    public partial class Xor128
    {
        public UInt32 X { get; set; }
        public UInt32 Y { get; set; }
        public UInt32 Z { get; set; }
        public UInt32 W { get; set; }

        public Xor128()
        {
            var bytes = Guid.NewGuid().ToByteArray();

            var handle = GCHandle.Alloc(bytes, GCHandleType.Pinned);

            try
            {
                var ptr = handle.AddrOfPinnedObject();

                X = (UInt32)Marshal.ReadInt32(ptr, 0x0);
                Y = (UInt32)Marshal.ReadInt32(ptr, 0x4);
                Z = (UInt32)Marshal.ReadInt32(ptr, 0x8);
                W = (UInt32)Marshal.ReadInt32(ptr, 0xC);
            }
            finally
            {
                handle.Free();
            }

            InitZiggurat();
        }
        public Xor128(UInt32 x, UInt32 y, UInt32 z, UInt32 w)
        {
            X = x;
            Y = y;
            Z = z;
            W = w;

            InitZiggurat();
        }

        public Int32[] GetWheel(Int32 size)
        {
            var result = new Int32[size];

            for (int i = 0; i < result.Length; i++)
            {
                result[i] = i;
            }

            for (int i = 0; i < result.Length; i++)
            {
                var j = UniformUInt32() % result.Length;

                var temp = result[i];
                result[i] = result[j];
                result[j] = temp;
            }

            return result;
        }

        public UInt32 UniformUInt32()
        {
            UInt32 t = X ^ (X << 11);

            X = Y;
            Y = Z;
            Z = W;

            return W = W ^ (W >> 19) ^ (t ^ (t >> 8));
        }
        public UInt32[] UniformUInt32Array(Int32 count)
        {
            UInt32[] result = new UInt32[count];

            for (Int32 i = 0; i < result.Length; i++)
            {
                UInt32 t = X ^ (X << 11);

                X = Y;
                Y = Z;
                Z = W;

                result[i] = (W = W ^ (W >> 19) ^ (t ^ (t >> 8)));
            }

            return result;
        }

        public Single UniformSingle()
        {
            UInt32 t = X ^ (X << 11);

            X = Y;
            Y = Z;
            Z = W;

            return ((Single)(W = W ^ (W >> 19) ^ (t ^ (t >> 8)))) / UInt32.MaxValue;
        }
        public Single UniformSingle(Single a, Single b)
        {
            Single factor = (b - a) / UInt32.MaxValue;

            UInt32 t = X ^ (X << 11);

            X = Y;
            Y = Z;
            Z = W;

            return a + ((Single)(W = W ^ (W >> 19) ^ (t ^ (t >> 8)))) * factor;
        }

        public Double UniformDouble()
        {
            UInt32 t = X ^ (X << 11);

            X = Y;
            Y = Z;
            Z = W;

            return ((double)(W = W ^ (W >> 19) ^ (t ^ (t >> 8)))) / UInt32.MaxValue;
        }
        public Double UniformDouble(Double a, Double b)
        {
            Double factor = (b - a) / UInt32.MaxValue;

            UInt32 t = X ^ (X << 11);

            X = Y;
            Y = Z;
            Z = W;

            return a + ((Double)(W = W ^ (W >> 19) ^ (t ^ (t >> 8)))) * factor;
        }

        public Byte[] UniformByteArray(Int32 count)
        {
            var result = new Byte[count];

            var handle = GCHandle.Alloc(result, GCHandleType.Pinned);
            var ptr = handle.AddrOfPinnedObject();

            try
            {
                for (Int32 i = 0; i < count; i += 4)
                {
                    Marshal.WriteInt32(ptr, i, (Int32)UniformUInt32());
                }

                var residuals = count & 0x03;

                if (residuals > 0)
                {
                    var value = (Int32)UniformUInt32();

                    for (Int32 i = 0; i < residuals; i++)
                    {
                        Marshal.WriteByte(ptr, count - i - 1, (Byte)(value & Byte.MaxValue));

                        value >>= 8;
                    }
                }

                return result;
            }
            finally
            {
                handle.Free();
            }
        }

        public Single[] UniformSingleArray(Int32 count)
        {
            Single[] result = new Single[count];

            for (Int32 i = 0; i < count; i++)
            {
                UInt32 t = X ^ (X << 11);

                X = Y;
                Y = Z;
                Z = W;

                result[i] = ((Single)(W = W ^ (W >> 19) ^ (t ^ (t >> 8)))) / UInt32.MaxValue;
            }

            return result;
        }
        public Single[] UniformSingleArray(Int32 count, Single a, Single b)
        {
            Single factor = (b - a) / UInt32.MaxValue;

            Single[] result = new Single[count];

            for (Int32 i = 0; i < count; i++)
            {
                UInt32 t = X ^ (X << 11);

                X = Y;
                Y = Z;
                Z = W;

                result[i] = a + ((Single)(W = W ^ (W >> 19) ^ (t ^ (t >> 8)))) * factor;
            }

            return result;
        }

        public Double[] UniformDoubleArray(Int32 count)
        {
            double[] result = new double[count];

            for (Int32 i = 0; i < count; i++)
            {
                UInt32 t = X ^ (X << 11);

                X = Y;
                Y = Z;
                Z = W;

                result[i] = ((double)(W = W ^ (W >> 19) ^ (t ^ (t >> 8)))) / UInt32.MaxValue;
            }

            return result;
        }

        UInt32 bytes;
        UInt32 bytesLeft;
        
        public Byte UniformByte()
        {
            if (bytesLeft < 1)
            {
                bytes = UniformUInt32();
                bytesLeft = 3;

                return (Byte)(bytes);
            }

            bytesLeft--;

            return (Byte)(bytes >> 8);
        }

        public void Wheel<T>(T[] wheel)
        {
            for (int i = 0; i < wheel.Length; i++)
            {
                var j = UniformUInt32() % wheel.Length;

                var temp = wheel[i];
                wheel[i] = wheel[j];
                wheel[j] = temp;
            }
        }
    }
}