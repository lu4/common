using System;
using System.Text.RegularExpressions;

namespace Common
{
    public class Tag
    {
        public Tag(String pattern)
            : this(pattern, true)
        {

        }

        public Tag(String pattern, Boolean escape)
        {
            this.Escape = escape;
            this.Pattern = pattern;
        }

        public String Pattern { get; private set; }
        public Boolean Escape { get; private set; }

        public String GetRegexPattern()
        {
            return Escape ? Regex.Escape(Pattern) : Pattern;
        }
    }
}

