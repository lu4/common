﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    public class Property<T>
    {
        private T value;

        public virtual T Value
        {
            get
            {
                return value;
            }
            set
            {
                OnBeforeSet(value);

                var v = this.value;
                this.value = value;
                IsSet = true;

                OnAfterSet(v);
            }
        }

        public Boolean IsSet { get; private set; }

        public void Reset()
        {
            IsSet = false;
        }

        protected virtual void OnAfterSet(T oldValue)
        {
            if (AfterSet != null)
            {
                AfterSet(this, new OldValueEventArgs<T>(oldValue));
            }
        }
        protected virtual void OnBeforeSet(T newValue)
        {
            if (BeforeSet != null)
            {
                BeforeSet(this, new NewValueEventArgs<T>(newValue));
            }
        }

        public event EventHandler<OldValueEventArgs<T>> AfterSet;
        public event EventHandler<NewValueEventArgs<T>> BeforeSet;
    }

    public class DefaultValueProperty<T> : Property<T>
    {
        public DefaultValueProperty(T defaultValue)
        {
            this.DefaultValue = defaultValue;
        }

        public T DefaultValue { get; set; }

        public override T Value
        {
            get
            {
                return IsSet ? base.Value : this.DefaultValue;
            }
            set
            {
                base.Value = value;
            }
        }
    }

    public class NoDefaultValueProperty<T> : Property<T>
    {
        public override T Value
        {
            get
            {
                if (IsSet)
                {
                    return base.Value;
                }
                else
                {
                    throw new InvalidOperationException("Property value not set");
                }
            }
            set
            {
                base.Value = value;
            }
        }
    }
}
