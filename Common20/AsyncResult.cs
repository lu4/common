﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

// Apm = As
namespace Common
{
    // Inspired by http://msdn.microsoft.com/en-us/magazine/cc163467.aspx
    public abstract class AsyncResult : IAsyncResult
    {
        private const Int32 PendingState = 0;
        private const Int32 CompletedSynchronouslyState = 1;
        private const Int32 CompletedAsynchronouslyState = 2;

        private readonly Object asyncState;
        private readonly AsyncCallback asyncCallback;

        private Int32 completedState = PendingState;

        // Field that may or may not get set depending on usage
        private ManualResetEvent asyncWaitHandle;

        // Fields set when operation completes
        private Exception exception;

        protected AsyncResult(AsyncCallback asyncCallback, Object state)
        {
            this.asyncState = state;
            this.asyncCallback = asyncCallback;
        }

        protected Boolean SetAsCompleted(Exception exception, Boolean completedSynchronously)
        {
            // The completedState field MUST be set prior calling the callback
            var prevState = Interlocked.Exchange(ref completedState,
                                                 completedSynchronously
                                                     ? CompletedSynchronouslyState
                                                     : CompletedAsynchronouslyState);

            if (PendingState == prevState)
            {
                // Passing null for exception means no error occurred. 
                // This is the common case
                this.exception = exception;

                // If the event exists, set it
                if (asyncWaitHandle != null) asyncWaitHandle.Set();

                // If a callback method was set, call it
                if (asyncCallback != null) asyncCallback(this);

                return true;
            }

            return false;
        }

        public void EndInvoke()
        {
            // This method assumes that only 1 thread calls EndInvoke 
            // for this object
            if (!IsCompleted)
            {
                // If the operation isn't done, wait for it
                AsyncWaitHandle.WaitOne();
                AsyncWaitHandle.Close();
                asyncWaitHandle = null;  // Allow early GC
            }

            // Operation is done: if an exception occured, throw it
            if (exception != null) throw exception;
        }

        #region Implementation of IAsyncResult
        public Object AsyncState { get { return asyncState; } }

        public Boolean CompletedSynchronously
        {
            get
            {
                return Thread.VolatileRead(ref completedState) == CompletedSynchronouslyState;
            }
        }

        public WaitHandle AsyncWaitHandle
        {
            get
            {
                if (asyncWaitHandle == null)
                {
                    var done = IsCompleted;
                    var mre = new ManualResetEvent(done);
                    if (Interlocked.CompareExchange(ref asyncWaitHandle, mre, null) != null)
                    {
                        // Another thread created this object's event; dispose 
                        // the event we just created
                        mre.Close();
                    }
                    else
                    {
                        if (!done && IsCompleted)
                        {
                            // If the operation wasn't done when we created 
                            // the event but now it is done, set the event
                            asyncWaitHandle.Set();
                        }
                    }
                }
                return asyncWaitHandle;
            }
        }

        public Boolean IsCompleted
        {
            get
            {
                return Thread.VolatileRead(ref completedState) != PendingState;
            }
        }
        #endregion
    }

    public class AsyncResultVoid : AsyncResult
    {
        public AsyncResultVoid(AsyncCallback asyncCallback, Object state)
            : base(asyncCallback, state)
        {
        }

        public Boolean SetAsCompleted(Boolean completedSynchronously)
        {
            return SetAsCompleted(null, completedSynchronously);
        }
        public Boolean SetAsFailed(Exception exception, Boolean completedSynchronously)
        {
            return SetAsCompleted(exception, completedSynchronously);
        }
    }

    public class AsyncResult<Result> : AsyncResult
    {
        // Field set when operation completes
        private Result result;

        public AsyncResult(AsyncCallback asyncCallback, Object state) :
            base(asyncCallback, state)
        {
        }

        public Boolean SetAsCompleted(Result result, Boolean completedSynchronously)
        {
            // Tell the base class that the operation completed 
            // sucessfully (no exception)
            this.result = result;

            return SetAsCompleted(null, completedSynchronously);
        }
        public Boolean SetAsFailed(Exception exception, Boolean completedSynchronously)
        {
            return SetAsCompleted(exception, completedSynchronously);
        }

        new public Result EndInvoke()
        {
            base.EndInvoke(); // Wait until operation has completed

            return result;  // Return the result (if above didn't throw)
        }
    }
}
