﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public static class Extensions35CP
    {
        public static T CloneObject<T>(this T obj) where T : ICloneable
        {
            return ((T)(obj.Clone()));
        }
        public static Byte[] NextBytes(this Random random, Int32 count)
        {
            Byte[] result = new Byte[count];

            random.NextBytes(result);

            return result;
        }
    }
}
